Extended syntax
uBO extends ABP filter syntax.

_ (aka "noop")
* (aka "all URLs")
$1p ($first-party)
$3p ($third-party)
$all (all network-based types + $popup + $document + $inline-font + $inline-script)
$badfilter
$css ($stylesheet)
$cname
$denyallow
$document
$domain ($from)
$elemhide ($ehide)
$font
$frame ($subdocument)
$genericblock Not supported
$generichide ($ghide)
$header
$image
$important
$inline-script
$inline-font
$match-case
$media
$method
$object
$other
$permissions
$ping
$popunder
$popup
$script
$specifichide ($shide)
$strict1p
$strict3p
$to
$webrtc example.com##+js(nowebrtc)
$websocket
$xhr ($xmlhttprequest)
$csp
$empty ($redirect=empty)
$mp4 ($redirect=noopmp4-1s)
$redirect
$redirect-rule
$removeparam
