Advanced settings
 

Index:
The "Advanced settings" page contains settings that are experimental, or that are of interest to advanced users who want more control over how uBlock Origin (uBO) behaves internally. There is no warranty those settings or any combination of them will work, they may change or be removed at any time. If some customizations in there are found to interfere negatively, it's for the user to address. Some of these settings are left undocumented, on purpose -- do not open issues about these.

These advanced settings can be easily accessed only when the setting "I am an advanced user" in the Settings pane in the dashboard is checked, but will persist and work even when the setting "I am an advanced user" is not checked:

Click on the cogs icon

The advanced settings available are described below. Be aware that those settings may change or be removed in the future, or more may be added.

If you want to reset a specific setting to its default value, just delete the value, uBO will fill the missing value with the default one.

If you want to reset all settings to their default values, delete everything then press "Apply changes".

Important: Some advanced settings are purposefully left undocumented, they are for internal use during development, or their long-term availability has not yet been decided. Do not open an issue about these purposefully undocumented settings -- the issue will be closed without further comment.

allowGenericProceduralFilters
Default: false.

uBO 1.20.0 and above.

If set to true, generic procedural cosmetic filters will no longer be discarded as invalid.

Whenever this setting is toggled, the user is responsible for forcing a reload of all filter lists so as to allow uBO to process differently any existing generic procedural cosmetic filters.

assetFetchTimeout
Default: 30 seconds.

The number of seconds after which uBO throws an error when a remote server fails to respond to a request.

autoCommentFilterTemplate
Default: {{date}} {{origin}}.

uBO 1.17.7b2 and above.

Template used to create comments in My filters when a new filter is created by Element Picker or from The Logger.

Placeholders are identified by {{...}}. Currently supported placeholders:

{{date}}: will be replaced with the current date
{{time}}: will be replaced with the current time
{{hostname}}: will be replaced with the hostname of the URL address for which the filter(s) was created
{{origin}}: will be replaced by the origin part of the address for which the filter(s) was created
{{url}}: new in 1.30.3b6, will be replaced by the full URL of the page for which a filter is created in Element Picker. For filters added from The Logger, replaced by the origin part of the URL.
If no placeholder is found in autoCommentFilterTemplate, this will disable auto-commenting. So one can use - or none to disable auto-commenting.

autoUpdateAssetFetchPeriod
Default: 15 seconds.

Before 1.53.1b1 60 seconds.
Before 1.39.3b0 120 seconds.

When the auto-updater kicks in and an asset in need of an update is fetched, this is the number of seconds to wait before fetching the next asset which needs to be updated. The delay helps spread the load on CPU and memory as a result of loading/parsing/compiling the filter lists which have been updated.

autoUpdateDelayAfterLaunch
Default: 105 secs.

Before 1.39.3b0 180 seconds.

uBO 1.20.0 and above.

The number of seconds to wait after launch before an auto-update session[1] is started.

[1] "Update session" means that uBO will lookup and update assets deemed out of date if any.

autoUpdatePeriod
Default: 1 hour.

Before 1.53.1b1: 2 hours.
Before 1.48.1b7: 4 hours.
Before 1.32.5rc0: 7 hours.

The time to wait in hours between each update session. uBO will always start an update session a few minutes after launch when auto-update is enabled. Once that first update session is completed, uBO will wait autoUpdatePeriod hours before starting a new update session.

benchmarkDatasetURL
Default: unset.

uBO 1.25.2 and above.

URL from where the benchmark dataset will be fetched. This allows launching the benchmark operations from within published versions of uBO, rather than from just a locally built version.

The most recent dataset to use is found at https://github.com/mjethani/scaling-palm-tree/: download locally requests.json as a raw text file.

Use a local URL to refer to the downloaded dataset. In a Chromium-based browser, you can use a file:///-based URL to the local copy. On Firefox you will have to create a simple HTTP server and create a URL such as http://localhost:8000/[...] to the local copy.

To launch benchmark operations, go to Support pane in the dashboard, then click the More button. This will open a new tab with various developer tools, one of which is SFNE: Benchmark, which purpose is to benchmark the static network filtering engine using the data at the URL provided above, against the currently selected filter lists.

blockingProfiles
Default: 11111/#F00 11010/#C0F 11001/#00F 00001

Before 1.31.3b13: 11111/#F00 11011/#C0F 11001/#00F 00001 (reload action bit for 3p is set)
Before 1.22.0: 11101 11001 00001

Introduced in 1.21.0, improved after 1.22.0 to reflect blocking mode in the color of uBO icon badge.
1.31.3b13 will attempt to automatically reload CSS styles without reloading the whole page when 3p blocking is relaxed.

Preference allows configuring cascade of the "Relax blocking mode" keyboard shortcut, along with corresponding badge color.

Default value contains four codes separated by space representing four blocking modes:

Hard mode +
No scripting	Medium mode +
No scripting	Medium mode	Default
11111/#F00	11011/#C0F	11001/#00F	00001
red badge	purple badge	blue badge	black badge
Each code consist of bit field indicating status of the feature ( 1 for blocked/enabled, 0 for not blocked/disabled ) and optional separator / followed by CSS color value[1].

3p-frame	3p-script	3p	no-scripting	reload action	separator	CSS color value[1]
Pressing "Relax blocking mode" will compare current uBO status with blockingProfiles codes from left to right and apply if it's different, then reload page if "reload action" is enabled, then apply color to the uBO icon badge.

[1]: The color CSS data type

cacheControlForFirefox1376932
Default: no-cache, no-store, must-revalidate.

uBO 1.17.0 and above.

Configure how uBO should affect caching main document requests for the purpose of dealing with browser bugs (see #229).

Possible values:

no-cache, no-store, must-revalidate:

Undesirable side effect: documents themselves for which uBO has to inject CSP directives as a result of filters/ruleset won't be available offline.
no-cache:

Undesirable side effect: One will need to explicitly cache-bypass reload a page each time uBO has to inject CSP directives as a result of filters/ruleset. Note that such cache-bypass reload does not affect only the document itself, but also all secondary resources inside that document.
unset:

Available after 1.22.0, turns off cache header modifications.
Related Firefox issues:

test-only patch: https://bugzilla.mozilla.org/show_bug.cgi?id=1376932
which fails permanently and was disabled: https://bugzilla.mozilla.org/show_bug.cgi?id=1573511.
https://bugzilla.mozilla.org/show_bug.cgi?id=1648635
cacheStorageAPI
Default: unset.

After 1.18.6 works in Firefox.

If set to browser.storage.local, uBO will use WebExtensions storage as a backend to cache storage.

Additionally, should indexedDB not be available for whatever reason, uBO will automatically fallback to browser.storage.local.

uBO 1.18.2 and above, Chromium only.

If set to indexedDB, uBO will use IndexedDB as a backend to the cache storage, potentially increasing performance and reducing memory usage. See #328 for details. Bad side effects - filter lists will be out of date in Chrome incognito mode - #399.

If unset, uBO will use whatever backend storage is optimal for the current platform.

cacheStorageCompression
Default: true.

uBO 1.16.21 and above.

If set to true, uBO will lz4-compress data before storing it in its cache storage. The cache storage is used for storing downloaded filter lists, compiled filter lists, selfies. This setting work when IndexedDB is used as the cache storage backend (by default with Firefox/Firefox for Android). See #141 for related discussion.

cloudStorageCompression
Default: true, was false before 1.30.9rc2

Introduced in 1.29.3b7.

Enable data compression before sending the data to cloud storage.

cnameIgnore1stParty
Default: true.

Introduced in 1.25.0.

Firefox only.

Whether uBO should ignore to re-run a network request through the filtering engine when the CNAME hostname is 1st-party to the alias hostname.

cnameIgnoreExceptions
Default: true.

Introduced in 1.25.0.

Firefox only.

Whether to bypass the uncloaking of network requests which were excepted by filters/rules.

This is necessary so as to avoid undue breakage by having exception filters being rendered useless as a result of CNAME-uncloaking.

For example, google-analytics.com uncloaks to www-google-analytics.l.google.com and both hostnames appear in Peter Lowe's list, which means exception filters for google-analytics.com (to fix site breakage) would be rendered useless as the uncloaking would cause the network request to be ultimately blocked.

cnameIgnoreList
Default: unset.

Introduced in 1.25.0.

Firefox only.

Possible values:

Space-separated list of hostnames.
* - all hostnames.
This tells uBO to NOT re-run the network request through uBO's filtering engine with the CNAME hostname.

This is useful to exclude commonly used actual hostnames from being re-run through uBO's filtering engine, so as to avoid pointless overhead.

cnameIgnoreRootDocument
Default: true.

Introduced in 1.25.0.

Firefox only.

Tells uBO to skip CNAME-lookup for root document.

cnameMaxTTL
Default: 120 minutes.

Introduced in 1.25.0.

Firefox only.

This tells uBO to clear its CNAME cache after the specified time.

For efficiency purposes, uBO will cache alias=>CNAME associations for reuse so as to reduce calls to browser.dns.resolve. All the associations will be cleared after the specified time to ensure the map does not grow too large and to ensure uBO uses up-to-date CNAME information.

cnameReplayFullURL
Default: false.

Introduced in 1.25.0.

Firefox only.

Tells uBO whether after CNAME-uncloaking replay through the filtering engine the whole URL or just the origin part of it.

Replaying only the origin part is meant to lower undue breakage and improve performance by avoiding repeating the pattern-matching of the whole URL -- which pattern-matching was most likely already accomplished with the original request.

cnameUncloakProxied
Default: false.

Introduced in 1.26.0.

Firefox only.

By default (set to false) uBO will no longer cname-uncloak when it detects that network requests are being proxied.

This default behavior can be overridden by setting this option to true.

This new advanced setting may disappear once the following Firefox issue is fixed: https://bugzilla.mozilla.org/show_bug.cgi?id=1618271

consoleLogLevel
Default: unset.

uBO 1.18.5b1 and above

For development purposes only.

If set to info, prints debug messages to the browser console.

debugScriptlets
debugScriptletInjector
Default: false.

If set to true, debugger; statement will be inserted just before scriptlet or scriptlet injector code.

disableWebAssembly
Default: false.

uBO 1.17.3rc4 and above.

For development purposes only.

If set to true, turns off WebAssembly optimizations in uBO code.

extensionUpdateForceReload
Default: false.

uBO 1.23.0 and above.

If set to true, restores update behavior from before 1.22.3b ("Prevent uBO from being reloaded mid-session "), the extension will unconditionally reload when an update is available; otherwise, the extension will reload only when being explicitly disabled then enabled, or when the browser is restarted.

filterAuthorMode
Default: false.

New in 1.47.5b7 - when set to true opening the context menu now shows a "View source code..." option.

After 1.28.0, the ability to point-and-click to create allow rules from the popup panel is now disabled by default.
Enabling this advanced setting restores its old functionality. Alternatively, you can tap twice on Ctrl to access allow rules only temporarily.

1.23.0-1.45.3rc1.
loggerPopupType
Default: popup.

uBO 1.23.0 and above.

Control the type of window to be used when the logger is launched as a separate window. Introduced to solve issues with missing/disabled titlebar buttons, resizing, incorrect drawing (#663).

Possible values:

popup - browser window without toolbars (default)
normal - normal browser window with all toolbars and buttons
any other value defined in Chromium or Firefox documentation.
manualUpdateAssetFetchPeriod
Default: 500 milliseconds.

When clicking the "Update now" button in the "3rd-party filters" pane in the dashboard, this is the number of milliseconds to wait before fetching the next asset which needs to be updated. The delay helps spread the load incurred as a result of loading/processing new filter lists, and its purpose is also to be considerate to remote servers by not subjecting them to rapid-fire requests.

modifyWebextFlavor
Default: unset.

Introduced in 1.38.7b3.

Value: A list of space-separated tokens to be added/removed from the computed default webext flavor.

The primary purpose is to give filter list authors the ability to test mobile flavor on desktop computers. Though mobile versions of web pages can be emulated using browser dev tools, it's not possible to do so for uBO itself.

Using +mobile as a value for this setting will force uBO to act as if it's being executed on a mobile device.

Important: this setting is best used in a dedicated browser profile, as this affects how filter lists are compiled. So best to set it in a new browser profile, then force all filter lists to be recompiled, and use the profile in the future when there is a need to test the specific webext flavor.

popupFontSize
Default: unset.

A valid CSS font size value (14px) to use for the popup panel. Use if you are unhappy with the default size.

popupPanelHeightMode
Default: 0.

uBO 1.28.0 and above.

Set to 1 to force the height of the firewall pane to be no more than the height of the basic pane.

Solves clipping of the Overview Panel - #785 (comment).

requestJournalProcessPeriod
Default: 1000 milliseconds.

uBO 1.16.21b2 and above.

Controls the delay before uBO internally processes its network request journal queue. The network request journal queue exists for the purpose of fixing issue 2053.

As a benign side effect to the fix, there is a delay in displaying the number of blocked requests on the extension icon (see #155).

A lower delay than the default one could bring back the issue it's meant to fix.

selfieAfter
Default: 2 minutes.

uBO 1.18.6 and above. Used to be 3 minutes until 1.39.3b0, and 11 minutes until 1.20.0.

Number of minutes after which selfie (optimized, internal representation of filters) is created.

strictBlockingBypassDuration
Default: 120 seconds.

uBO 1.17.3b4 and above.

Controls duration of the Strict blocking "Temporarily" bypass.

trustedListPrefixes
Default: ublock- in stable release, ublock- user- in development version.

uBO 1.52.3rc2 and above

Space-separated list of tokens specifying which lists are considered trustworthy.

Examples of possible values:

ublock-: trust only uBO lists, exclude everything else including content of My filters (default value)
ublock- user-: trust uBO lists and content of My filters
-: trust no list, essentially disabling all filters requiring trust (admins or people who don't trust us may want to use this)
One can also decide to trust lists maintained elsewhere. For example, for stock AdGuard lists add  adguard-. To trust stock EasyList lists, add  easylist-.

To trust a specific regional stock list, look-up its token in assets.json and add to trustedListPrefixes.

The matching is made with String.startsWith(), hence why ublock- matches all uBO's own filter lists.

This also allows to trust imported lists, for example add ` https://filters.adtidy.org/extension/ublock/filters/ to trust all non-stock AdGuard lists.

Add the complete URL of a given imported list to trust only that one list.

URLs not starting with https:// or file:/// will be rejected, i.e. http://example.org/ will be ignored.

Invalid URLs are rejected.

uiPopupConfig
Default: unset, was undocumented before 1.39.2.

Introduced in 1.27.0.

Override uBO popup interface configuration.

The value can be one or more space-separated tokens:

+captions/-captions: enable/disable captions
+no-popups/-no-popups: enable/disable no-popups switch
+logger/-logger: new in 1.39.1b1, enable/disable the logger button
uiStyles
Default: unset.

uBO 1.28.0 and above.

Allow bypassing uBO's default CSS styles in case they are causing issues to specific users. It is the responsibility of the user to ensure the value of uiStyles contains valid CSS property declarations. uBO will assign the value to document.body.style.cssText.

For example, in the case of the issue #1044, one could set uiStyles to font-family: sans-serif to force uBO to the system font for its user interface. Another example is issue #1254 wherein uBO v1.30.0 will be possible to adjust width of popup panels.

updateAssetBypassBrowserCache
Default: false

With 1.22.0 and above.

If set to true, uBO will ensure the browser cache is bypassed when manually1.27.0 fetching a remote resource more often than every hour.

This is for the convenience of filter list maintainers who may want to test the latest version of their lists when fetched from their remote location.

userResourcesLocation
Default: unset.

One or more space-separated URLs which content will be parsed as token-identified resources to be used for redirect or scriptlet-injection (+js(...)) purpose.

Screenshot from 2023-04-08 13-25-44

uBO expects valid content such as can be seen in legacy version of scriptlets.js, anything else will lead to undefined results.

Any duplicate as per token will result in the previous resource being replaced by the latter one. The resource files are loaded in order of URL appearance, and uBO stock resource file is always loaded first.

Additional resources will be updated at the same time the built-in resource file is updated. Purging the cache of 'uBlock filters' will also purge the cache of the built-in resource file -- and hence force a reload of user-specified resources if any.

The setting was introduced in 1.12.0. Support for multiple URLs was introduced in 1.19.0.

